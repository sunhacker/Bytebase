---
title: How to setup 'project' ?
---

- Bytebase project is similar to the project concept in other common dev tools.

- A project has its own members, and every issue and database always belongs to a single project.

- A project can also be configured to link to a repository to enable version control workflow.
