---
title: How to setup 'Database' ?
---

- Each Bytebase database maps to the one created by 'CREATE DATABASE xxx'. In Bytebase, a database always belongs to a single project.

- Bytebase will periodically sync the database info for every recorded instance. You can also create a new database from the dashboard.
